package com.busyqa.wk1;

public class DataTypeAndOperator {
    public static void main(String[] args) {


        demoOperations();
        demoOperations2();
        demoOperations3();


    }



    //Operator Precedence
    public static void demoOperations() {
        int x = -1;
        float y = 3.0f;
        char z = 'x';
        String z1 = "x";
        boolean t = true;

        // ========= addition
        x = x + 1;//(1)
        x += 1;//(2)

        // ======== Increment
        x = 1;
//        x++;//(3a)
        System.out.println(x++);
        System.out.println(x);

        // =========
        x = x + 1;
        System.out.println(x);

        x = 1;
//        ++x;//(3b)
        System.out.println(++x);

    }


    //Operator Precedence
    public static void demoOperations2() {
        int num1 = 10;
        int num2 = 15;
        int num3 = 25;
        boolean num4 = true;

        System.out.println((num1 + num2) - num3 / 5);

        // ++i vs i++
        System.out.println(num1++);
        System.out.println(++num2);

        System.out.println(num2 < num3 && num1 > num3);
        System.out.println(!num4);

    }

    public static void demoOperations3() {

        boolean z = 4 < 5; //true
        boolean y = 1.1 > 5.5; //false
        System.out.println(z || y);
        System.out.println(z && y);
        System.out.println(!(z && y));

    }
    public static void demoIfStatement() {
        float x_123 = 5 / 2;

        float y = 5 % 2;
        System.out.println(x_123);
        System.out.println(y);
        System.out.println(y == x_123);

        if (x_123 <= 65) {
            System.out.println("yes");
        } else if (x_123 <= 18) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
}
