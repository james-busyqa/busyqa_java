package com.busyqa.wk1;

public class KeywordContinueBreakReturn {
    public static void main(String args[]) {

//        testLoop();
//        testSwitch(1);
        testSwitch(5);
    }

    public static void testLoop() {
        int sum = 0;

        for (int x = 1; x <= 10; x++) {
            System.out.print("\n Value of x:" + x);
//            if (x == 2) {
//                continue;
//            }
            if (x == 7) {
                break;
            }
            sum += x;
            System.out.print("| End of each loop");
            System.out.print("sum = " + sum);
        }
        System.out.println("\n Value of sum:" + sum);

        // ========
        if (sum > 1) {
            return;
        }
        System.out.println("End of Method");
    }

    public static void testSwitch(int month) {
        System.out.println();

        String monthString;
        switch (month) {
            case 1:
                monthString = "January";
            case 2:
                monthString = "February";
                break;
            case 3:
                monthString = "March";
                break;
            case 4:
                monthString = "April";
                break;
            case 5:
                monthString = "May";
            case 6:
                monthString = "June";
            case 7:
                monthString = "July";
            case 8:
                monthString = "August";
            case 9:
                monthString = "September";
            case 10:
                monthString = "October";
            case 11:
                monthString = "November";
            case 12:
                monthString = "December";
                break;
            default:
                monthString = "Invalid month";
        }
        System.out.println(monthString);
    }

}
