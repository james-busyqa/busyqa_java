package com.busyqa.wk2.buildin;

public class ValuePassing2 {
    public static void main(String[] Args) {
        arrayPassing();
    }
    // ================================================ (2) Array and pass by value and reference
    static void printInt(int myInt) {
        myInt++;
        System.out.println("printInt:" + myInt);

    }

    static void printInt(Integer myInt) {
        myInt++;
        System.out.println("printObjInt:" + myInt);

    }

    static void printInt(TestClass myInt) {
        myInt.testInt++;
        System.out.println("printClass:" + myInt.testInt);

    }

    static void printInt(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] + 1;
            System.out.println(arr[i]);
        }
    }

    public static void arrayPassing() {
        //======== pass by value
        int anInt = 10;
        printInt(anInt);
        printInt(anInt);

        //======== pass by value
        Integer objInt = 10;
        printInt(objInt);
        printInt(objInt);

        //======== pass by reference
        TestClass testClass = new TestClass(10);
        printInt(testClass);
        printInt(testClass);

        //======== pass by reference
        int a[] = {33, 3, 4, 5};//declaration, instantiation and initialization
        printInt(a);
        printInt(a);
        printInt(a);

        //======== passing anonymous array to method
        printInt(new int[]{10, 22, 44, 66});
    }
}

class TestClass {
    public int testInt;

    public TestClass(int testInt) {
        this.testInt = testInt;
    }
}