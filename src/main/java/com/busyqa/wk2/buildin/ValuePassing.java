package com.busyqa.wk2.buildin;

public class ValuePassing {
    public static void main(String[] args) {
        Dog aDog = new Dog("Max");
        foo(aDog);
        // when foo(...) returns, the name of the dog has been changed to "Fifi"
        System.out.println(aDog.getName().equals("Fifi")); // true
    }

    public static void foo(Dog d) {
        System.out.println(d.getName().equals("Max"));// true
        // this changes the name of d to be "Fifi"
        d.setName("Fifi");
    }
}

class Dog {
    private String name;

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
