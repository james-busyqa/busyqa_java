package com.busyqa.wk2.buildin;

public class Enum {
    public static void main(String arg[]) {

        // switch case
        System.out.println(testDays(DayType.Monday));


//        DayType dtExample1 = new DayType();
        DayType dtExample = DayType.Monday;
        System.out.println(dtExample);

        // get single value
        System.out.println("get the value of: " + DayType.Monday);

        // get all value by foreach loop
        for (DayType dt : DayType.values()) {
            System.out.println(dt.toString());
        }

    }


    public static String testDays(DayType day) {
        switch (day) {
            case Monday:
                return "Its Monday!!";


            case Tuesday:
                return "Its Tuesday";

            case Thursday:
                return "Its Thursday";
            case Wednesday:
                return "Its Wednesday";
            default:
                return "Rest of the week....";
        }
    }
}

enum DayType {
    Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
}

class DayTypeOld extends Object {
    final public static int MONDAY = 1;
    final public static int TUESDAY = 2;
    final public static int WEDNESDAY = 3;
    final public static int THURSEDAY = 4;
    final public static int FRIEDAY = 5;
    final public static int SATURDAY = 6;
    final public static int SUNDAY = 7;

    public static String testSwitch(int numDay) {
        String output;
        switch (numDay) {
            case MONDAY:
                output = "MONDAY";
                break;
            case TUESDAY:
                output = "TUESDAY";
                break;
            default:
                output = "otherdya";
        }
        return output;

    }
}

