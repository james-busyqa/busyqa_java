package com.busyqa.wk2.buildin;


import java.util.Arrays;

public class StringStuff {

    public static void main(String[] arg) {


//        char c = 'cf';

        // initialization
        String str1 = "abc";
        String str2 = new String("abc");

        // comparision
        System.out.println(str1.trim().equals(str2.trim()));
        System.out.println(str1 == str2);

        // concat
        String str3 = str1.concat(str2);
        String str4 = str1 + str2;

        // StringBuilder
        String str = str1;
        str = str + str2;
        str = str + str3;
        str = str + str4;


        StringBuilder sb = new StringBuilder();
        sb.append(str1);
        sb.append(str2);
        sb.append(str3);
        sb.append(str4);
        String finalString = sb.toString();
        System.out.println(finalString);

        // replace
        str = str1.replace('n', 't');
        String str22 = String.valueOf("new     ").substring(0, 3);


        // string ---> array
        str1 = "1,2,3,4";
        String[] arrStr = str1.split(",");
        System.out.println(Arrays.asList(arrStr));

    }

//    public static void m1() {
//
//    }
//
//
//    public static int m2_getDefaultSal() {
//        int returnValue = 100;
//        return returnValue;
//    }
//
//    public static void m3(String str) {
//        String result = "";
//        str = str.concat("new");
//        str = str + "new";
//
//        System.out.print(str);
//
//    }
//
//    public static String m4(String str) {
//
//
//        System.out.print(str.charAt(0));
//
//        if (str.endsWith(".")) {
//            str = String.valueOf(str.charAt(0)).toUpperCase()
//                    + str.substring(1, str.length() - 1);
//
//            //change the first letter to upcase
//        }
//        return str;
//    }
}





