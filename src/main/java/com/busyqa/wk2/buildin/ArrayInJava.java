package com.busyqa.wk2.buildin;

import java.util.ArrayList;

public class ArrayInJava {
    public static void main(String[] Args) {

    }

    public static void arrayInit1() {
        int c = 3;

        Integer b = new Integer(1);

        ArrayList<Integer> d =new ArrayList();
        d.add(1);
        d.add(2);
        d.add(3);
        d.remove(0);

        int a[] = new int[5];//declaration and instantiation
        a[0] = 10;//initialization
        a[1] = 20;
        a[2] = 70;
        a[3] = 40;
        a[4] = 50;

        int a2[] = new int[10];//declaration and instantiation


        //traversing array
        for (int i = 0; i < a.length; i++)//length is the property of array
            System.out.println(a[i]);
    }


    public static void arrayInit2() {
        int a[] = {33, 3, 4, 5};//declaration, instantiation and initialization
        //printing array
        for (int i = 0; i < a.length; i++)//length is the property of array
            System.out.println(a[i]);

        a[0] = 5;

    }

    public static void arrayInit3() {
        // Anonymous declaration
        System.out.println((new int[] {33, 3, 4, 5}).length);
    }



}


