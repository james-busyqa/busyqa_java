package com.busyqa.wk2.modifier.packageDefault;

public class AnotherPackgeDefault {
    /* Since we didn't mention any access com.busyqa.wk2.modifier here, it would
     * be considered as default.
     */
    int addTwoNumbers(int a, int b){
        return a+b;
    }
}
