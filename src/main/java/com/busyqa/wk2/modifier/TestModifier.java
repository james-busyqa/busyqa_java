package com.busyqa.wk2.modifier;

import com.busyqa.wk2.modifier.packageDefault.AnotherPackgeDefault;
import com.busyqa.wk2.modifier.packageProtect.AnotherPackgeProtect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class TestModifier extends AnotherPackgeProtect {
    public static void main(String args[]) throws NoSuchFieldException, IllegalAccessException {
        // ======== default-packge: the two classes must be in the same package, so you can use it.
        AnotherPackgeDefault obj = new AnotherPackgeDefault();
        //  obj.addTwoNumbers(10, 21);


        // ======== private: no acceptability for other class
        InFileNestClass obj2 = new InFileNestClass();
        //System.out.println(obj2.num);
        // System.out.println(obj2.square(10));

        // reflection: access private by reflecting the code
        InFileNestClass privateObject = new InFileNestClass();
        Field privateStringField = InFileNestClass.class.getDeclaredField("num");
        privateStringField.setAccessible(true);
        double fieldValue = (double) privateStringField.get(privateObject);
        System.out.println("fieldValue = " + fieldValue);


        // ======== protect
        //use it though inheritance
        TestModifier obj3 = new TestModifier();
        System.out.println(obj3.addTwoNumbers(11, 22));

        //without inheritance: you can't use protected variable or method
        AnotherPackgeProtect obj4 = new AnotherPackgeProtect();
        // System.out.println(obj4.addTwoNumbers(11, 22));
    }
}

class InFileNestClass {
    // ======== private: no acceptability for other class, but you can use it inside the class
    private double num = 100;
    private int square(int a) {
        return a * a;
    }
}