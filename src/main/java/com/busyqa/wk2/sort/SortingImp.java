package com.busyqa.wk2.sort;


import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

public class SortingImp {

//    static ArrayList<String> myStrList = new ArrayList<String>();
//    static ArrayList<Integer> myIntList = new ArrayList<Integer>();

    static String[] myStrArray = {"b", "a", "B", "A"};


    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> myIntegerList = random.ints(10, 1, 100).
                boxed().collect(Collectors.toList());


        System.out.println("Before Collections.sort:" + myIntegerList);
        Collections.sort(myIntegerList); //default ASC
        System.out.println("After Collections.sort:" + myIntegerList);


        // ========
        Collections.shuffle(myIntegerList);
        System.out.println("Before implemented comparator:" + myIntegerList);
        Collections.sort(myIntegerList, (o1, o2) -> {
            if (o1 > o2) {
                return 1;
            } else if (o1 < o2) {
                return -1;
            } else {
                return 0;
            }
        });
        System.out.println("After implemented comparator:" + myIntegerList);
        // ===
        //comparable: interface for class, override compareTo
        //comparator: interface for sort method, override compare
        // ASC: o1>o2 ---> return positive
        // ASC: o1<o2 ---> return negative
        // ASC: o1>o2 ---> return 0


        // ========
        Collections.shuffle(myIntegerList);
        System.out.println("Before reverseOrder:" + myIntegerList);
        Collections.sort(myIntegerList, Collections.reverseOrder()); //default ASC
        System.out.println("After reverseOrder:" + myIntegerList);
    }
}

