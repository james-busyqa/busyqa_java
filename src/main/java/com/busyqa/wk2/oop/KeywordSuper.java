package com.busyqa.wk2.oop;

class ParentClass {
    int abc = 0;

    //Parent class constructor
    ParentClass() {
        System.out.println("Constructor of Parent");
    }

    void disp() {
        System.out.println("Parent Method");
    }
}

class ChildClass extends ParentClass {
    int abc = 10;   // override/ hide the parent

    ChildClass(int a) {
        super();
        System.out.println("Constructor of Child");

    }

    void disp() {

        System.out.println("Child Method");

        super.abc = 100;
        System.out.println("child: " + this.abc + "; parent.abc: " + super.abc);

        //Calling the disp() method of parent class
        super.disp();
    }

}

public class KeywordSuper {


    public static void main(String args[]) {
        //Creating the object of child class
        ChildClass obj = new ChildClass(1);
        obj.disp();
    }
}