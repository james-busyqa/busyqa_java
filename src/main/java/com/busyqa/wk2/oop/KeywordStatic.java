package com.busyqa.wk2.oop;

public class KeywordStatic {

    public static void main(String[] args) {
//        Demo1 d1 = new Demo1();
//        System.out.print(d1.nonStaticVariable2);
//        Demo1.staticMethodDemo();

        Demo2.staticM1();
        ////////////
        Demo2 d2 = new Demo2();
        d2.method2();
        d2.staticM1();
        System.out.print(Demo2.staticA);

    }
}

class Demo1 {
    // ======== fields for demo
    static int staticVariable1 = 10;
    int nonStaticVariable2 = 20;

    // ======== static method for demo
    static void staticMethod1() {
        String v1 = new String();
        v1 = "wetw";
    }

    // ======== none static method for demo
    void nonStaticMethod2() {
    }

    // ======== test static method capability
    static void staticMethodDemo() {
        //in static: you can only use static field and method unless you initialize an object

        //can call static field & method
        staticVariable1 = 99;
        staticMethod1();

        //can't call instance field & method
//        Demo1.nonStaticMethod2 = 99;


        //must initiate
        Demo1 d1 = new Demo1();
        d1.nonStaticVariable2 = 99;
        d1.nonStaticMethod2();

        //alternative
        (new Demo1()).nonStaticVariable2 = 99;
        (new Demo1()).nonStaticMethod2();
    }


    // ======== test none static method capability
    void nonStaticMethodDemo() {
        //in none static: you can use this and super keywords, and static field and method
        this.nonStaticMethod2();
        Demo2.staticM1();

        //can call instance field & method
        staticVariable1 = 99;
        staticMethod1();
        this.nonStaticVariable2 = 99;
        this.nonStaticMethod2();
    }


}

// a simplified example
class Demo2 {
    static int staticA = 10;
    int nonStaticB = 20;

    static void staticM1() {
        //in static: you can only use static field and method unless you initialize an object
//        nonStaticB = 100;
        System.out.println("Demo2.staticMethod1()");
    }

    void method2() {
        System.out.println("Demo2.staticM2()");
    }
}