package com.busyqa.wk2.oop.newpackage;

public class ClassInner {
    public static void main(String args[]) {
        // (1) init: Instantiating the outer class
        Outer_Demo outer = new Outer_Demo();

        // (2) init: Instantiating the inner class
        Outer_Demo.Inner_Demo inner = outer.new Inner_Demo();
        System.out.println(inner.getNum());



        // (3) init: Instantiating the AnonymousInner class (no modifier)
        Nameofit nameofit = new Nameofit();
        AnonymousInner anonymousInner = new AnonymousInner() {
            public void mymethod() {

            }
        };
        anonymousInner.mymethod();

        // (4) init: Static Nested Class
        ClassInner.Nested_Demo nested = new ClassInner.Nested_Demo();
        nested.my_method();
    }

    // (4) def: Static Nested Class (can have modifier)
    //    public static class Nested_Demo {
    static class Nested_Demo {
        public void my_method() {
            System.out.println("This is my nested class");
        }
    }

    // (5) instance method of the outer class (no modifier)
    void my_Method() {
        int num = 23;

        // (5) def: method-local inner class
        class MethodInner_Demo {
            public void print() {
                System.out.println("This is method inner class " + num);
            }
        } // end of inner class

        // (5) init: Accessing the inner class
        MethodInner_Demo inner = new MethodInner_Demo();
        inner.print();
    }


}

// (1) def: outer class but in the same file
class Outer_Demo {
    // private variable of the outer class
    private int num = 175;

    // (2) def: inner class
    public class Inner_Demo {
        public int getNum() {
            System.out.println("This is the getnum method of the inner class");
            return num;
        }
    }
}

// (3) def:
abstract class AnonymousInner {
    public abstract void mymethod();

    public void fwleifj() {

    }
}

class Nameofit extends AnonymousInner{

    @Override
    public void mymethod() {

    }
}
// (3) def:
interface AnonymousInnerf {
    void mymethod();
}
