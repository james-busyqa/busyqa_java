package com.busyqa.wk2.oop.newpackage;

import java.util.Arrays;

public class ClassEncapsulateConstructor {
    public static void main(String[] arg) {

        // ======== Demonstration of inheritance and initialization
        Employee emp1;
        emp1 = new Employee();
        emp1.eid = 1020;
        emp1.ename = "John";
        emp1.sal = 80000;
        emp1.job = "Manager";
        emp1.display();
        emp1.action();

        EmployeeOfRD emp2;
        emp2 = new EmployeeOfRD();
        emp2.action();

        EmployeeOfHr emp3;
        emp3 = new EmployeeOfHr(342);
        emp3.display();
        emp3.bonus();
        Person person = emp3;
        person.action();
        showEmploee(emp2);

        // ======== Demonstration of static utility method
        String aNumberString = String.valueOf(1);
        System.out.println((new int[]{33, 3, 4, 5}));
        System.out.println(Arrays.toString((new int[]{33, 3, 4, 5})));

        // ======== Demonstration of Object array
        Employee a[] = new Employee[5];//declaration and instantiation
        a[0] = new Employee();//initialization
        a[1] = new Employee();
        a[2] = new Employee();
        a[3] = new Employee();
        a[3].ename = "James1";
        a[4] = new Employee();
        a[4].ename = "James";

        // ======== Demonstration of implementation of equals
        System.out.println(a[3].equals(a[4]));
    }

    public static void showEmploee(Employee e) {
        System.out.print(e.ename);

    }
}


// ======== inheritance ========
// Multiple inheritance (by interface): Person, Entrepreneur ---> Boss
// Multiple layer inheritance: Person ---> AbstractEmployee ---> Employee ---> EmployeeOfHr

//interface: only abstract method, no method body
//abstract: partially finished, some methods without body
//class: every method has to complete implement

// ================================================================
interface Person {
    void action();
}

interface Entrepreneur {
    void embition();
}

class Boss implements Entrepreneur, Person {
    int money;

    @Override
    public void action() {
        System.out.print("earning profit");
    }

    @Override
    public void embition() {
        System.out.print("earning significant profit");
    }
}

abstract class AbstractEmployee implements Person {
    abstract void otherAction();
    public void action() {
        System.out.print("work hard");
    }
}

class Employee extends AbstractEmployee {

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Employee) {
            if (this.ename.toUpperCase().equals(((Employee) obj).ename.toUpperCase())) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public Employee(String type) {
        this.type = type;
        if (type.isEmpty()) {
            System.out.println("your input is empty, default is general");
            type = "general employee";
        }
    }

    void otherAction() {

    }

    public Employee() {
        System.out.println("From Employee");
    }

    public Employee(double sal) {
        this.sal = sal;
    }


    // Demonstrate source code generate constructor and set get
    String type = "normal employee";
    String d1;
    int eid;
    String ename;
    double sal;
    String job;


    public void action(int a, String b) {
        System.out.print("work hard");
    }

    void display() {
        System.out.println(type);
//        System.out.println(eid);
//        System.out.println(ename);
//        System.out.println(sal);
//        System.out.println(job);
    }

    void bonus() {
        double result = 0;
        if (sal < 5000) {
            result = (sal * 20) / 100;
        } else {
            result = (sal * 10) / 100;
        }
        System.out.println(result);
    }


}

class EmployeeOfHr extends Employee {

    public EmployeeOfHr(int sal) {
        System.out.println("From EmployeeOfHr");
    }

    String type = "HR";

    Duty duty;

    void bonus() {
        System.out.println((sal * 5) / 100);
    }

}

class EmployeeOfRD extends Employee {

    public EmployeeOfRD() {
        System.out.println("From EmployeeOfRD");

    }

    String type = "RD";

    Skill skill;
    Duty duty;


    @java.lang.Override
    void bonus() {
        System.out.println((sal * 20) / 100);

    }

    void display() {
        System.out.println(type);
    }

    String display2() {
        return "";
    }
}


// ======== object inside the object
class Duty {
    String[] dutyArr;
}

class Skill {
    String s1;
}