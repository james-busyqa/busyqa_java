package com.busyqa.wk2.oop;

public class KeywordThis {
    int c, b;

    void add1(int a, int b) {
        c = a;
        b = b;  // still assign to local b
    }

    void add2(int a, int b) {
        this.c = a;
        this.b = b; // assign to member variable b
    }

    void display() {
//        int b = 999;
        System.out.println("c=" + c);
        System.out.println("b=" + b);           // if there is no local variable, member variable will be used
    }

    public static void main(String[] args) {
        KeywordThis cal0 = new KeywordThis();
        cal0.display();

        KeywordThis cal = new KeywordThis();
        cal.add1(10, 20);
        cal.display();
//
        cal.add2(10, 20);
        cal.display();
    }
}

