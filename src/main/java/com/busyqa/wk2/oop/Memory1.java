package com.busyqa.wk2.oop;

public class Memory1 {
    public static void main(String[] args) {
        int id = 23;
        String pName = "Jon";
        Person p = null;
        p = new Person(id, pName);
    }
}

class Person {
    int pid;
    String name;

    public Person(int pid, String name) {
        this.pid = pid;
        this.name = name;
    }

}

