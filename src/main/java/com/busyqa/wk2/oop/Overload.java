package com.busyqa.wk2.oop;


class OverloadSample {
    public void disp(char c) {
        System.out.println(c);
    }

    public void disp(char c, int num) {
        System.out.println(c + " " + num);
    }

    public void disp(char c, String num) {
        System.out.println(c + " " + num);
    }

    // number or type of argument must be different, not the return type
//    public int disp(char c) {
//        System.out.println(c + " " + num);
//    }
}


public class Overload {
    public static void main(String args[]) {
        String str = new String ();
        String str2 = new String ("weofji");


        OverloadSample obj = new OverloadSample();
        obj.disp('a');
        obj.disp('a', 10);
    }
}