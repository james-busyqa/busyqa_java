package com.busyqa.wk2.oop;


interface ShapeViewPoint {

}

interface extandedShapeViewPoint extends ShapeViewPoint {
    final static short va1 = 1;

    float volume1();
}

interface AnotherViewPoint {
    final static short va2 = 2;

    float volume2();
}

class Cube implements extandedShapeViewPoint, AnotherViewPoint {
    public int add() {
        return 1;
    }

    public float volume1() {
        return 5.0f;
    }

    public float volume2() {
        return 0;
    }
}

public class ClassPolymorphism {
    public static void main(String[] args) {
        Cube cude = new Cube();
        methodUseType1(cude);
        methodUseType2(cude);

        ShapeViewPoint fake = new ShapeViewPoint(){};
        methodUseType1(fake);
    }

    // ========  different view gets different field and method
    public static void methodUseType1(ShapeViewPoint s) {

        // ======== from ShapeViewPoint, you can't use va1 and va2
//         System.out.println(s.va1);
//         System.out.println(s.va2);

        // ======== from ShapeViewPoint, you can't use volume1
//         float x = s.volume1();

        // ======== alternative change the view on the fly
        if (s instanceof Cube) {
            float y = ((Cube) s).volume1();
        }

        System.out.println(s instanceof Cube);
    }

    public static void methodUseType2(AnotherViewPoint s) {

        // ======== from AnotherViewPoint, you can't use va1
        //  System.out.println(s.va1);
        System.out.println(s.va2);
    }


}
