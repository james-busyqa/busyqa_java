package com.busyqa.wk2.oop;

import java.util.ArrayList;

public class Memory {
    public static void main(String[] args) {
        ArrayList<String> myList = new ArrayList<>();
        myList.add(new String("Apple"));
        myList.add(new String("Boll"));
        myList.add(new String("Computer"));
        printList(myList, "");

        // ======== pass by value and pass by reference
        // === by reference: the value of the List will be changed
        passByReference(myList);
        printList(myList, " passed by reference");

        // === by value: the value of the primitive will not be changed
        passByValue(myList.get(0));
        printList(myList, " passed by value");


        // ========
        // === by value: the value will not be changed
        WrapperClass wrapperClass = new WrapperClass();
        passByValue(wrapperClass.strValue);
        printClass(wrapperClass, " passed by value");

        // === by value: the value will not be changed
        WrapperClass wrapperClass3 = new WrapperClass();
        passByValue(wrapperClass.intValue);
        printClass(wrapperClass3, " passed by value");

        // === by reference: the value of the wrapped object will be changed
        WrapperClass wrapperClass2 = new WrapperClass();
        passByReference(wrapperClass2);
        printClass(wrapperClass2, " passed by reference");


        // ========
        // === by value: the value of the bear object will not be changed
        String strValue = "AString";
        passByValue(strValue);
        printValue(strValue);

        // === by value: the value of the bear value will not be changed
        int intValue = 999;
        passByValue(intValue);
        printValue(intValue);

        // ========
        int[] intArr = new int[]{1, 2, 3};
        printArray(intArr, " original");
        passByReference(intArr);
        printArray(intArr, " passed by reference");
    }

    public static void printList(ArrayList<String> data, String note) {
        System.out.println("========The list is: " + note);
        data.forEach(System.out::println);
    }

    public static <T> void printClass(WrapperClass data, String note) {
        System.out.println("========The class is: " + note);
        System.out.println(data.strValue);
        System.out.println(data.intValue);
    }

    public static void printArray(int[] data, String note) {
        System.out.println("========The array is: " + note);
        for (int i : data) {
            System.out.println(i);
        }
    }

    public static <T> void printValue(T data) {
        System.out.println("========The value is: " + data);
    }


    // ================================================================

    public static void passByReference(ArrayList<String> data) {
        data.set(0, "Asus");
        data.add("Desk");
    }

    public static void passByReference(WrapperClass data) {
        data.strValue = "Changed";
        data.intValue = 000;
    }

    public static void passByValue(String data) {
        data = "Zoo";
    }

    public static void passByValue(int data) {
        data = 000;
    }

    public static void passByReference(int[] data) {
        data[0] = 0;
        data[1] = 0;
        data[2] = 0;
    }
}

// ================================================================
class WrapperClass {
    public String strValue = "AString";
    public int intValue = 888;
}