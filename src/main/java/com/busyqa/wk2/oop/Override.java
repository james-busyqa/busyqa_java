package com.busyqa.wk2.oop;

public class Override {
    public static void main(String[] args) {
        // If a Parent type reference refers to a Parent object, resolveMain Parent's show is called
        Parent obj1 = new Parent();
        obj1.show();

        // If a Parent type reference refers to a Child object Child's show() is called.
        Child obj2 = new Child();
        obj2.show();
        obj2.show("something");
    }
}

// A Simple Java program to demonstrate method overriding in java

// Base Class
class Parent {
    void show() {
        System.out.println("Parent's show()");
    }
}

// Inherited class
class Child extends Parent {
    // This method overrides show() of Parent
//    void show() {
//        System.out.println("Child's show()");
//    }



    // This method is overload
    void show(String input) {
        System.out.println("Overload show()");
        return;
    }

    // This method is overload, but the argument is the same as others, so it is incorrect.
//    String show() {
//        System.out.println("Parent's show()");
//        return "";
//    }
}
